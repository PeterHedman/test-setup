#!/bin/bash -eu
OPT_INSTANCES='1'
OPT_NAME='test-node'

HELP() {
    echo "Usage: $0 "
    echo "optional parameters"
    echo "-i <nr of vms>    default: 1"
    echo "-n <name>         default: test-node"
    echo "-h print this help message"
    exit 1;
}

while getopts ":i:n:h" arg; do
    case "${arg}" in
        i)
            OPT_INSTANCES=${OPTARG}
            ;;
        n)
            OPT_NAME=${OPTARG}
            ;;
        h)
            HELP
            ;;
        *)
            HELP
            ;;
    esac
done
shift $((OPTIND-1))

for (( i=1; i<=$OPT_INSTANCES; i++ ))
do
    docker stop $OPT_NAME-$i
    docker rm $OPT_NAME-$i
    rm -rf $OPT_NAME-$i/
    ADDR=$(docker inspect "$OPT_NAME-$i" | grep '"IPAddress":' | head -1)
#    if [[ $(ssh-keygen -F $OPT_NAME-$i) ]]; then
#        ssh-keygen -f "~/.ssh/known_hosts" -R "$ADDR"
#    fi
done
