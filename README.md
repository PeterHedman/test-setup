# Test setup

This projects aims to utilize the vm-launcher container to provide
 the means to easily orchestrate a cluster of machines for testing
 purposes.

## Prerequsites

It is required to have docker and qemu-img installed for this script
to work. Also if they are setup to require sudo this script will also
 need to run as sudo. 

## Usage

```
./start-infra.sh

 optional parameters
-f <vm-image_url> default: http://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
-i <nr of vms>    default: 1
-n <name>         default: test-node
-u <vm_user>      default: obald
-p <user pw>      default: obald
-m <vm_memory>    default: 1024
-c <vm_cpu>       default: 1
-d <vm_disk>      default: 5G
-k <ssh key file> default: ~/.ssh/id_rsa.pub
-V <nested_virt>  
-B <uefi_boot>    
-h print this help message
```

## Example

Start 3 fedora vm instances with 8G disk, myname/mypassword login and the names test-k8s-1...3

```
./start-infra.sh  \
  -f https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2 \
  -i 3 -n test-k8s -u myname -p mypassword -d 8G
```

## Example urls for different vm images
-fedora28 https://download.fedoraproject.org/pub/fedora/linux/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.qcow2

-fedora27 https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2

-CentOS7 http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1808.qcow2

-Debian9 http://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd65.qcow2

-openSUSE http://download.opensuse.org/repositories/Cloud:/Images:/Leap_15.0/images/openSUSE-Leap-15.0-OpenStack.x86_64-0.0.4-Buildlp150.12.25.qcow2

-Ubuntu Xenial http://cloud-images.ubuntu.com/xenial/20180921/xenial-server-cloudimg-amd64-disk1.img
