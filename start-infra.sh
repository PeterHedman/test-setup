#!/bin/bash

OPT_VM_URL="http://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
OPT_PW="obald"
OPT_USER="obald"
OPT_MEMORY="1024"
OPT_CPU="1"
OPT_KVM_RESOURCES="-m $OPT_MEMORY -smp $OPT_CPU,sockets=$OPT_CPU,cores=1,threads=1"
OPT_DISK="5G"
OPT_NAME="test-node"
OPT_INSTANCES='1'
OPT_SSH_KEY='~/.ssh/id_rsa.pub'
OPT_NESTED_HVM='N'
OPT_UEFI_BOOT='N'

HELP() {
    echo "Usage: $0 "
    echo "optional parameters"
    echo "-f <vm-image_url> default: http://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
    echo "-i <nr of vms>    default: 1"
    echo "-n <name>         default: test-node"
    echo "-u <vm_user>      default: obald"
    echo "-p <user pw>      default: obald"
    echo "-m <vm_memory>    default: 1024"
    echo "-c <vm_cpu>       default: 1"
    echo "-d <vm_disk>      default: 5G"
    echo "-k <ssh key file> default: ~/.ssh/id_rsa.pub"
    echo "-V <nested_virt>  "
    echo "-B <uefi_boot>    "  
    echo "-h print this help message"
    echo ""
    echo "--Example urls for different vm images"
    echo "CentOS7       http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1808.qcow2"
    echo "Debian9       http://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd65.qcow2"
    echo "openSUSE      http://download.opensuse.org/repositories/Cloud:/Images:/Leap_15.0/images/openSUSE-Leap-15.0-OpenStack.x86_64-0.0.4-Buildlp150.12.25.qcow2"
    echo "Ubuntu Xenial http://cloud-images.ubuntu.com/xenial/20180921/xenial-server-cloudimg-amd64-disk1.img"
    echo "Fedora        https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2"
    echo "--Example command:"
    echo "./start-infra.sh -f https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2 -i 3 -n mytestnode -u myname -p mypassword -d 20G"
    exit 1;
}

while getopts ":f:i:n:p:u:m:c:d:k:VBh" arg; do
    case "${arg}" in
        f)
	    echo "VM_URL         :${OPTARG}"
	    OPT_VM_URL=${OPTARG}
            ;;
        i)
	    echo "NR_OF_INSTANCES:${OPTARG}"
            OPT_INSTANCES=${OPTARG}
            ;;
        n)
	    echo "NAME           :${OPTARG}"
            OPT_NAME=${OPTARG}
            ;;
        p)
	    echo "PASSWORD       :****"
            OPT_PW=${OPTARG}
            ;;
        u)
	    echo "USER           :${OPTARG}"
            OPT_USER=${OPTARG}
            ;;
        m)
	    echo "MEMORY         :${OPTARG}"
            OPT_MEMORY=${OPTARG}
            ;;
        c)
	    echo "CPU            :${OPTARG}"
            OPT_CPU=${OPTARG}
            ;;
        d)
	    echo "DISK           :${OPTARG}"
            OPT_DISK=${OPTARG}
            ;;
	k)
	    echo "SSH_KEY_PATH   :${OPTARG}"
	    OPT_SSH_KEY=${OPTARG}
	    ;;
        V)
	    echo "Enabled Nested HVM"
            OPT_NESTED_HVM='Y'
            ;;
        B)
	    echo "Using UEFI boot"
            OPT_UEFI_BOOT='Y'
            ;;
        h)
            HELP
            ;;
        *)
            HELP
            ;;
    esac
done
shift $((OPTIND-1))

#check if required commands are available

if [[ ! -x "$(command -v qemu-img)" ]]; then
    echo "qemu-img required please install"
    exit 2
elif [[ ! -x "$(command -v docker)" ]]; then
    echo "docker required please install"
    exit 2
elif [[ ! -x "$(command -v wget)" ]]; then
    echo "wget required please install"
    exit 2
else
    echo "Preparing test env setup..."
fi

if [[ -f "$OPT_SSH_KEY" ]]; then
    PUBLIC_KEY=$(cat "$OPT_SSH_KEY")
else
    PUBLIC_KEY=""
fi

: ${LOCK_PW:='false'}
: ${ALLOW_PW:='True'}
#: ${PWHASH:="$(openssl passwd -1 -salt xyz $OPT_PW)"}
OPT_KVM_CPU_OPTS="-m $OPT_MEMORY -smp $OPT_CPU,sockets=$OPT_CPU,cores=1,threads=1"

export RAW_USER=$(cat <<EOF
#cloud-config
users:
  - default
  - name: $OPT_USER
    passwd: $(openssl passwd -1 -salt xyz $OPT_PW)
    groups: sudo
    lock_passwd: $LOCK_PW
    ssh_pwauth: $ALLOW_PW
    shell: /bin/bash
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    chpasswd: {expire: False}
    ssh_authorized_keys:
      - $PUBLIC_KEY
runcmd:
  - sudo sed -i 's/127.0.0.1 localhost/127.0.0.1 localhost localhost.localdomain/g' /etc/hosts
final_message: "vm-container finally up after $UPTIME"
EOF
)

VM_IMAGE_NAME=${OPT_VM_URL##*/}

# Get the ubuntu 16.04 image to use
if [[ ! -f "/tmp/$VM_IMAGE_NAME" ]]; then
    wget $OPT_VM_URL -O /tmp/$VM_IMAGE_NAME
else
    echo "$VM_IMAGE_NAME already downloaded"
fi
# extend size as specified
if [[ "$OPT_DISK" != '0' ]]; then
    sudo qemu-img resize /tmp/$VM_IMAGE_NAME $OPT_DISK
fi

for (( i=1; i<=$OPT_INSTANCES; i++ ))
do
    export RAW_META=$(cat <<EOF
{
  "availability_zone": "",
  "hostname": "$OPT_NAME-$i",
  "uuid": "$i$i$i$i$i"
}
EOF
)

    DIR=$(pwd)/$OPT_NAME-$i
    mkdir $DIR
    cp /tmp/$VM_IMAGE_NAME $DIR/image

    # Start VM
    docker run -itd --privileged \
	    -v $DIR:/image \
	    -e CLOUD_INIT_TYPE='RAW_config' \
	    -e USE_NET_BRIDGES='0' \
	    -e RAW_META \
	    -e AUTO_ATTACH='yes' \
	    -e KVM_RESOURCES="$OPT_KVM_CPU_OPTS" \
	    -e RAW_USER \
	    -e VM_PUBLIC_KEY="$PUBLIC_KEY" \
	    -e REMOTE_IMAGE='false' \
	    -e NESTED_HVM="$OPT_NESTED_HVM" \
	    -e UEFI_BOOT="$OPT_UEFI_BOOT" \
	    --name="$OPT_NAME-$i" \
	    obald/vm-launcher:latest

    # print IP address to access via ssh
    ADDR="$(docker inspect "$OPT_NAME-$i" | grep '"IPAddress":' | head -1)"
    echo "VM started with $ADDR"
done
